﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using EOSLib.ExternalTools;

namespace Launcher
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            dtStart.Value = DateTime.Today;
            dtEnd.Value = DateTime.Today;
        }

        private async void btnLaunch_Click(object sender, EventArgs e)
        {
            btnLaunch.Enabled = false;
            var loader = new ExternalToolsLoader();
            var files = GetFiles();
            pbLoad.Maximum = files.Count;
            pbLoad.Value = 0;
            pbLoad.Step = 1;
            lbLog.Items.Add($"{DateTime.Now} : Загрузка начата.");
            foreach (var filePath in files)
            {
                try
                {
                    lbLog.Items.Add($"{DateTime.Now} : Загружается {Path.GetFileNameWithoutExtension(filePath)}");
                    await loader.LoadZipAsync(filePath);
                    lbLog.Items.Add($"{DateTime.Now} : Загрузка файла завершена.");
                }
                catch (Exception ex)
                {
                    lbLog.Items.Add($"{DateTime.Now} : В процессе загрузки произошла ошибка: {ex.Message}");
                }
                finally
                {
                    pbLoad.Value++;
                }
            }

            lbLog.Items.Add($"{DateTime.Now} : Загрузка завершена");
            MessageBox.Show("Загрузка завершена.", "Загрузка данных", MessageBoxButtons.OK);
            btnLaunch.Enabled = true;
        }

        private List<string> GetFiles()
        {
            var startDate = dtStart.Value;
            var endDate = dtEnd.Value;
            var result = new List<string>();
            while (startDate <= endDate)
            {
                var filePath = Path.Combine(tbPath.Text, $"Extraction_Tool_{startDate:yyyy.MM.dd}.zip");
                if (File.Exists(filePath))
                    result.Add(filePath);
                startDate = startDate.AddDays(1);
            }

            return result;
        }
    }
}

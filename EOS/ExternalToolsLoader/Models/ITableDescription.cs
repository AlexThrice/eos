﻿using System;
using System.Collections.Generic;
using NpgsqlTypes;

namespace EOSLib.Models
{
    public interface ITableDescription
    {
        string TableName { get; }

        IReadOnlyDictionary<string, NpgsqlDbType> Fields { get; }

        string GetCopySql();

        string GetDeleteSql();
    }
}
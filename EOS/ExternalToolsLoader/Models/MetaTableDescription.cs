﻿using System;
using System.Collections.Generic;
using NpgsqlTypes;

namespace EOSLib.Models
{
    public class MetaTableDescription : ITableDescription
    {
        public MetaTableDescription()
        {
            Fields = new Dictionary<string, NpgsqlDbType>
            {
                {"filename",NpgsqlDbType.Varchar},
                {"filedate",NpgsqlDbType.Timestamp},
                {"starttime",NpgsqlDbType.Varchar},
                {"duration",NpgsqlDbType.Varchar},
                {"ansi",NpgsqlDbType.Varchar},
                {"dnis",NpgsqlDbType.Varchar},
                {"agentpbxid",NpgsqlDbType.Varchar},
                {"agentid",NpgsqlDbType.Varchar},
                {"cd1",NpgsqlDbType.Varchar},
                {"cd2",NpgsqlDbType.Varchar},
                {"cd3",NpgsqlDbType.Varchar},
                {"cd4",NpgsqlDbType.Varchar},
                {"cd5",NpgsqlDbType.Varchar},
                {"cd6",NpgsqlDbType.Varchar},
                {"cd7",NpgsqlDbType.Varchar},
                {"cd8",NpgsqlDbType.Varchar},
                {"cd9",NpgsqlDbType.Varchar},
                {"cd10",NpgsqlDbType.Varchar},
                {"cd11",NpgsqlDbType.Varchar},
                {"cd12",NpgsqlDbType.Varchar},
                {"cd13",NpgsqlDbType.Varchar},
                {"cd14",NpgsqlDbType.Varchar},
                {"cd15",NpgsqlDbType.Varchar},
                {"cd16",NpgsqlDbType.Varchar},
                {"cd17",NpgsqlDbType.Varchar},
                {"cd18",NpgsqlDbType.Varchar},
                {"cd19",NpgsqlDbType.Varchar},
                {"cd20",NpgsqlDbType.Varchar},
                {"cd21",NpgsqlDbType.Varchar},
                {"cd22",NpgsqlDbType.Varchar},
                {"cd23",NpgsqlDbType.Varchar},
                {"cd24",NpgsqlDbType.Integer},
                {"cd25",NpgsqlDbType.Integer},
                {"cd26",NpgsqlDbType.Varchar},
                {"cd27",NpgsqlDbType.Varchar},
                {"cd28",NpgsqlDbType.Varchar},
                {"cd29",NpgsqlDbType.Varchar},
                {"cd30",NpgsqlDbType.Varchar},
                {"cd31",NpgsqlDbType.Varchar},
                {"cd32",NpgsqlDbType.Varchar},
                {"cd33",NpgsqlDbType.Varchar},
                {"cd34",NpgsqlDbType.Varchar},
                {"cd35",NpgsqlDbType.Varchar},
                {"cd36",NpgsqlDbType.Varchar},
                {"cd37",NpgsqlDbType.Varchar},
                {"cd38",NpgsqlDbType.Varchar},
                {"cd39",NpgsqlDbType.Varchar},
                {"cd40",NpgsqlDbType.Varchar},
                {"cd41",NpgsqlDbType.Varchar},
                {"cd42",NpgsqlDbType.Varchar},
                {"cd43",NpgsqlDbType.Varchar},
                {"cd44",NpgsqlDbType.Varchar},
                {"cd45",NpgsqlDbType.Varchar},
                {"cd46",NpgsqlDbType.Varchar},
                {"cd47",NpgsqlDbType.Varchar},
                {"cd48",NpgsqlDbType.Varchar},
                {"cd49",NpgsqlDbType.Integer},
                {"cd50",NpgsqlDbType.Integer},
                {"cd51",NpgsqlDbType.Varchar},
                {"cd52",NpgsqlDbType.Varchar},
                {"cd53",NpgsqlDbType.Varchar},
                {"cd54",NpgsqlDbType.Varchar},
                {"cd55",NpgsqlDbType.Varchar},
                {"cd56",NpgsqlDbType.Varchar},
                {"cd57",NpgsqlDbType.Varchar},
                {"cd58",NpgsqlDbType.Varchar},
                {"cd59",NpgsqlDbType.Varchar},
                {"cd60",NpgsqlDbType.Varchar},
                {"cd61",NpgsqlDbType.Varchar},
                {"cd62",NpgsqlDbType.Varchar},
                {"cd63",NpgsqlDbType.Varchar},
                {"cd64",NpgsqlDbType.Varchar},
                {"cd65",NpgsqlDbType.Varchar},
                {"cd66",NpgsqlDbType.Varchar},
                {"cd67",NpgsqlDbType.Varchar},
                {"cd68",NpgsqlDbType.Varchar},
                {"cd69",NpgsqlDbType.Varchar},
                {"cd70",NpgsqlDbType.Varchar},
                {"cd71",NpgsqlDbType.Varchar},
                {"cd72",NpgsqlDbType.Varchar},
                {"cd73",NpgsqlDbType.Varchar},
                {"cd74",NpgsqlDbType.Integer},
                {"cd75",NpgsqlDbType.Integer},
                {"pcd1",NpgsqlDbType.Integer},
                {"pcd2",NpgsqlDbType.Integer},
                {"pcd3",NpgsqlDbType.Integer},
                {"pcd4",NpgsqlDbType.Integer},
                {"pcd5",NpgsqlDbType.Integer},
                {"pcd6",NpgsqlDbType.Integer},
                {"pcd7",NpgsqlDbType.Integer},
                {"pcd8",NpgsqlDbType.Integer},
                {"pcd9",NpgsqlDbType.Integer},
                {"pcd10",NpgsqlDbType.Integer},
                {"pcd11",NpgsqlDbType.Integer},
                {"pcd12",NpgsqlDbType.Integer},
                {"pcd13",NpgsqlDbType.Integer},
                {"pcd14",NpgsqlDbType.Integer},
                {"pcd15",NpgsqlDbType.Integer},
                {"channel",NpgsqlDbType.Varchar},
                {"unit",NpgsqlDbType.Varchar},
                {"sid",NpgsqlDbType.Varchar},
                {"siteid",NpgsqlDbType.Varchar},
                {"screenunit",NpgsqlDbType.Varchar},
                {"numberofholds",NpgsqlDbType.Varchar},
                {"numberofconferences",NpgsqlDbType.Varchar},
                {"numberoftransfers",NpgsqlDbType.Varchar},
                {"totalholdtime",NpgsqlDbType.Varchar},
                {"localstarttime",NpgsqlDbType.Varchar},
                {"localendtime",NpgsqlDbType.Varchar},
                {"contactid",NpgsqlDbType.Varchar},
                {"wrapuptime",NpgsqlDbType.Varchar},
                {"switchid",NpgsqlDbType.Varchar},
                {"switchcallid",NpgsqlDbType.Varchar},
                {"dbs_id",NpgsqlDbType.Varchar},
                {"extension",NpgsqlDbType.Varchar},
                {"isexception",NpgsqlDbType.Varchar},
                {"stringextension",NpgsqlDbType.Varchar},
                {"total_agent_talk_time",NpgsqlDbType.Varchar},
                {"total_customer_talk_time",NpgsqlDbType.Varchar},
                {"total_silence_time",NpgsqlDbType.Varchar},
                {"total_talk_over_time",NpgsqlDbType.Varchar},
                {"num_agent_initiated_talk_over_segments",NpgsqlDbType.Varchar},
                {"num_customer_initiated_talk_over_segments",NpgsqlDbType.Varchar},
                {"agent_name",NpgsqlDbType.Varchar},
                {"xmlversion",NpgsqlDbType.Varchar},
                {"itshostname",NpgsqlDbType.Varchar},
                {"languagemodelversion",NpgsqlDbType.Varchar},
                {"transcriptiontime",NpgsqlDbType.Varchar}
            };
        }

        public string TableName => "T_EXTERNAL_TOOLS_META";

        public IReadOnlyDictionary<string, NpgsqlDbType> Fields { get; }

        public string GetCopySql()
        {
            return $"COPY {TableName} ({string.Join(",", Fields.Keys)}) FROM STDIN (FORMAT BINARY)";
        }

        public string GetDeleteSql()
        {
            return $"delete from {TableName} where filedate = @date";
        }
    }
}
﻿using System.Collections.Generic;
using NpgsqlTypes;

namespace EOSLib.Models
{
    public class InfoTableDescription : ITableDescription
    {
        public InfoTableDescription()
        {
            Fields = new Dictionary<string, NpgsqlDbType>
            {
                { "filename", NpgsqlDbType.Varchar },
                { "filedate", NpgsqlDbType.Timestamp },
                { "txt", NpgsqlDbType.Varchar },
                { "v1", NpgsqlDbType.Integer },
                { "v2", NpgsqlDbType.Integer },
                { "v3", NpgsqlDbType.Integer },
                { "v4", NpgsqlDbType.Integer },
            };
        }

        public string TableName => "T_EXTERNAL_TOOLS_INFO";

        public IReadOnlyDictionary<string, NpgsqlDbType> Fields { get; }

        public string GetCopySql()
        {
            return $"COPY {TableName} ({string.Join(",", Fields.Keys)}) FROM STDIN (FORMAT BINARY)";
        }

        public string GetDeleteSql()
        {
            return $"delete from {TableName} where filedate = @date";
        }
    }
}
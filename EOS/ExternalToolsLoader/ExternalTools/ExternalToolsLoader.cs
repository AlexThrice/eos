﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using EOSLib.Models;
using Npgsql;
using NpgsqlTypes;

namespace EOSLib.ExternalTools
{
    public class ExternalToolsLoader
    {
        private readonly ITableDescription _metaTable;
        private readonly ITableDescription _infoTable;

        public ExternalToolsLoader()
        {
            _metaTable = new MetaTableDescription();
            _infoTable = new InfoTableDescription();
        }

        public async Task LoadZipAsync(string fileName)
        {
            var dateStr = Path.GetFileNameWithoutExtension(fileName).Split('_').Last();
            var date = DateTime.ParseExact(dateStr, "yyyy.MM.dd", CultureInfo.InvariantCulture).Date;
            using (var connection = GetConnection())
            {
                await connection.OpenAsync();
                await CleanDataAsync(connection, _metaTable, date);
                await CleanDataAsync(connection, _infoTable, date);
                using (var archive = ZipFile.Open(fileName, ZipArchiveMode.Read))
                {
                    foreach (var entry in archive.Entries)
                    {
                        await LoadEntryAsync(entry, connection, date);
                    }
                }
            }
        }

        private async Task CleanDataAsync(NpgsqlConnection connection, ITableDescription tableDescription, DateTime fileDate)
        {
            using (var command = connection.CreateCommand())
            {
                command.CommandText = tableDescription.GetDeleteSql();
                command.Parameters.AddWithValue("date", fileDate);
                await command.ExecuteNonQueryAsync();
            }
        }

        private async Task LoadEntryAsync(ZipArchiveEntry entry, NpgsqlConnection connection, DateTime fileDate)
        {
            using (var stream = entry.Open())
            {
                if (entry.Name.EndsWith("info"))
                {
                    await LoadInfoFileAsync(stream, Path.GetFileNameWithoutExtension(entry.Name), connection, fileDate);
                }
                else if (entry.Name.EndsWith("meta"))
                {
                    await LoadMetaFileAsync(stream, Path.GetFileNameWithoutExtension(entry.Name), connection, fileDate);
                }
            }
        }

        private async Task LoadInfoFileAsync(Stream stream, string fileName, NpgsqlConnection connection, DateTime fileDate)
        {
            using (var reader = new StreamReader(stream))
            {
                var values = new List<string[]>();
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    values.Add(line?.Split('\t'));
                }
                await WriteDataToDbAsync(connection, _infoTable, fileDate, fileName, values);
            }
        }

        private async Task LoadMetaFileAsync(Stream stream, string fileName, NpgsqlConnection connection, DateTime fileDate)
        {
            var settings = new XmlReaderSettings { NameTable = new NameTable() };
            var xmlns = new XmlNamespaceManager(settings.NameTable);
            xmlns.AddNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");
            var context = new XmlParserContext(null, xmlns, "", XmlSpace.Default);
            using (var reader = XmlReader.Create(stream, settings, context))
            {
                var xmlDoc = new XmlDocument();
                xmlDoc.Load(reader);
                var rowValues = ParseXml(xmlDoc);
                var values = new List<string[]> {rowValues.ToArray()};
                await WriteDataToDbAsync(connection, _metaTable, fileDate, fileName, values);
            }
        }

        private static List<string> ParseXml(XmlNode doc)
        {
            var values = new List<string>();
            var parentNode = doc.SelectSingleNode("/CAudioFile/CRI");
            //if (parentNode != null)
            {
                values.Add(parentNode.SelectSingleNode("StartTime")?.InnerText);
                values.Add(parentNode.SelectSingleNode("Duration")?.InnerText);
                values.Add(parentNode.SelectSingleNode("ANI")?.InnerText);
                values.Add(parentNode.SelectSingleNode("DNIS")?.InnerText);
                values.Add(parentNode.SelectSingleNode("Direction")?.InnerText);
                values.Add(parentNode.SelectSingleNode("AgentPBXID")?.InnerText);
            }

            parentNode = parentNode.SelectSingleNode("PrivateData/PrivateData");
            //if (parentNode != null)
            {
                var childNodes = parentNode.ChildNodes;
                var cnt = childNodes.Count;
                for (var i = 0; i < cnt; i++)
                {
                    var childNode = childNodes[i];
                    var valueNode = childNode.FirstChild.NextSibling;
                    var value = valueNode?.InnerText;
                    values.Add(value == "" ? null : value);
                }
            }

            parentNode = doc.SelectSingleNode("/CAudioFile/CRI");
            //if (parentNode != null)
            {
                values.Add(parentNode.SelectSingleNode("Channel")?.InnerText);
                values.Add(parentNode.SelectSingleNode("Unit")?.InnerText);
                values.Add(parentNode.SelectSingleNode("SID")?.InnerText);
                values.Add(parentNode.SelectSingleNode("SiteID")?.InnerText);
                values.Add(parentNode.SelectSingleNode("ScreenUnit")?.InnerText);
                values.Add(parentNode.SelectSingleNode("NumberOfHolds")?.InnerText);
                values.Add(parentNode.SelectSingleNode("NumberOfConferences")?.InnerText);
                values.Add(parentNode.SelectSingleNode("NumberOfTransfers")?.InnerText);
                values.Add(parentNode.SelectSingleNode("TotalHoldTime")?.InnerText);
                values.Add(parentNode.SelectSingleNode("LocalStartTime")?.InnerText);
                values.Add(parentNode.SelectSingleNode("LocalEndTime")?.InnerText);
                values.Add(parentNode.SelectSingleNode("ContactID")?.InnerText);
                values.Add(parentNode.SelectSingleNode("WrapUpTime")?.InnerText);
                values.Add(parentNode.SelectSingleNode("SwitchID")?.InnerText);
                values.Add(parentNode.SelectSingleNode("SwitchCallID")?.InnerText);
                values.Add(parentNode.SelectSingleNode("DBS_ID")?.InnerText);
                values.Add(parentNode.SelectSingleNode("Extension")?.InnerText);
                values.Add(parentNode.SelectSingleNode("IsException")?.InnerText);
                values.Add(parentNode.SelectSingleNode("StringExtension")?.InnerText);
                values.Add(parentNode.SelectSingleNode("TOTAL_AGENT_TALK_TIME")?.InnerText);
                values.Add(parentNode.SelectSingleNode("TOTAL_CUSTOMER_TALK_TIME")?.InnerText);
                values.Add(parentNode.SelectSingleNode("TOTAL_SILENCE_TIME")?.InnerText);
                values.Add(parentNode.SelectSingleNode("TOTAL_TALK_OVER_TIME")?.InnerText);
                values.Add(parentNode.SelectSingleNode("NUM_AGENT_INITIATED_TALK_OVER_SEGMENTS")?.InnerText);
                values.Add(parentNode.SelectSingleNode("NUM_CUSTOMER_INITIATED_TALK_OVER_SEGMENTS")?.InnerText);
            }

            values.Add(doc.SelectSingleNode("/CAudioFile/Agent/Name")?.InnerText);
            values.Add(doc.SelectSingleNode("/CAudioFile/XmlVersion")?.InnerText);
            values.Add(doc.SelectSingleNode("/CAudioFile/ITSHostname")?.InnerText);
            values.Add(doc.SelectSingleNode("/CAudioFile/LanguageModelVersion")?.InnerText);
            values.Add(doc.SelectSingleNode("/CAudioFile/TranscriptionTime")?.InnerText);

            return values;
        }

        private static NpgsqlConnection GetConnection()
        {
            return new NpgsqlConnection("Server=localhost;Port=5432;Database=EOS_REPO;User Id=EOS_REPO;Password=EOS_REPO;CommandTimeout=600");
        }

        private static async Task WriteDataToDbAsync(NpgsqlConnection connection, 
                                                      ITableDescription tableDescription, 
                                                      DateTime fileDate, 
                                                      string fileName, 
                                                      IEnumerable<object[]> values)
        {
            using (var loader = connection.BeginBinaryImport(tableDescription.GetCopySql()))
            {
                var fieldTypes = tableDescription.Fields.Values.ToArray();
                foreach (var rowValue in values)
                {
                    await loader.StartRowAsync();
                    await loader.WriteAsync(fileName, NpgsqlDbType.Varchar);
                    await loader.WriteAsync(fileDate, NpgsqlDbType.Timestamp);
                    for (var i = 0; i < rowValue.Length; i++)
                    {
                        var value = rowValue[i];
                        if (value == null)
                        {
                            await loader.WriteNullAsync();
                        }
                        else
                        {
                            await loader.WriteAsync(ConvertValue(value, fieldTypes[i+2]), fieldTypes[i+2]);
                        }
                    }
                }
                loader.Complete();
            }
        }

        private static object ConvertValue(object value, NpgsqlDbType type)
        {
            return type == NpgsqlDbType.Integer ? int.Parse(value.ToString()) : value;
        }
    }
}
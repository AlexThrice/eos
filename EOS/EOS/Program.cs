﻿using System;
using EOSLib.ExternalTools;

namespace EOS
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(DateTime.Now);
            var tools = new ExternalToolsLoader();
            tools.LoadZipAsync(@"F:\Sasha\Выскребенцев\EOS\Extraction_Tool_2019.09.20.zip").Wait();
            Console.WriteLine(DateTime.Now);
            Console.ReadLine();
        }
    }
}
